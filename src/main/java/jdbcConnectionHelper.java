
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class jdbcConnectionHelper {
    private String jdbcUrl;
    private String username;
    private String password;

    //object from mysql's connection class
    protected Connection currentconnection;

    public jdbcConnectionHelper(String url, String username, String password) {
        this.jdbcUrl = url;
        this.username = username;
        this.password = password;
    }

    public String getUrl() {
        return jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "jdbcConnectionHelper{" +
                "url='" + jdbcUrl + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    //create connection
    public void jdbcConnection(){
        System.out.println("connected");
//        try {
//            currentconnection = DriverManager.getConnection(jdbcUrl, username,                 password);
//            System.out.println("Successfully connected");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }
}
