
public class jdbcConnectionBuilder {

    private String jdbcUrl;
    private String username;
    private String password;

    public jdbcConnectionBuilder setUrl(String url) {
        this.jdbcUrl = url;
        return this;

    }

    public jdbcConnectionBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public jdbcConnectionBuilder setPassword(String password) {
        this.password = password;
        return this;
    }
    public jdbcConnectionHelper build(){

        return new jdbcConnectionHelper(jdbcUrl, username, password);
    }


}